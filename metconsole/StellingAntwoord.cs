﻿using System;

namespace metconsole
{
    public class StellingAntwoord
    {
        public StellingAntwoord(bool isBeantwoord, bool isAkkoord, string argument, Gebruiker gebruiker, Stelling stelling)
        {
            this.isBeantwoord = isBeantwoord;
            this.isAkkoord = isAkkoord;
            Argument = argument;
            Gebruiker = gebruiker;
            Stelling = stelling;
        }

        public bool isBeantwoord { get; set; }
        public bool isAkkoord { get; set; }
        public string Argument { get; set; }
        public Gebruiker Gebruiker { get; set; }
        public Stelling Stelling { get; set; }
        
        
        
        
        //public Sessie Sessie { get; set; }
    }
}