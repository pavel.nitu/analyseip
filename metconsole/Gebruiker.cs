﻿namespace metconsole
{
    public class Gebruiker
    {
        public Stelling CurrentStelling { get; set; }
        public string Result { get; set; }
        
        public Sessie Sessie { get; set; }
        
        public int Id { get; set; }

        public void Antwoordt(Sessie s, bool akkord, bool isBeanwordt, string arg)
        {
            StellingAntwoord antwoord = new StellingAntwoord(isBeanwordt,akkord,arg,this,CurrentStelling);
            Sessie.ReceiveAntwoord(antwoord);
        }

        public void ReceiveResult(string result)
        {
            Result = result;
        }
    }
}