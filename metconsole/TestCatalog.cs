﻿using System.Collections.Generic;

namespace metconsole
{
    public class TestCatalog
    {
        public List<Test> Tests { get; set; }

        public Test GetTest(int testid)
        {
            return Tests.Find(t => t.TestId == testid);
        }
        
    }
}