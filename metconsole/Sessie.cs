﻿using System.Collections.Generic;

namespace metconsole
{
    public class Sessie
    {

        private int curentStellingPos = 0;
        public Stelling CurrentStelling { get; set; }
        
        private List<Gebruiker> Gebruikers { get; set; }
        
        public Sessie(SessieSpelMode sessieSpelMode, Test test)
        {
            SessieSpelMode = sessieSpelMode;
            Test = test;
        }


        public SessieSpelMode SessieSpelMode { get; set; }
        public Test Test { get; set; }
        public List<StellingAntwoord> StellingAntwoorden { get; set; }



        public string ShowResults()
        {
            if (SessieSpelMode == SessieSpelMode.Debat)
            {
                //resultaten tonen
            }
            else
            {
                
            }

            return "";
        }

        public void ReceiveAntwoord(StellingAntwoord stellingAntwoord)
        {
            StellingAntwoorden.Add(stellingAntwoord);
        }

        public void FinishSession()
        {
            //naar elke gebruiker zijn resultaat sturen
        }

        public void NextStelling()
        {
            if (curentStellingPos + 1 < Test.Stellingen.Count)
            {
                CurrentStelling = Test.Stellingen[curentStellingPos];
                NotifyGebruikers();
            }
        }

        public string ShowResultStelling()
        {
            int aantalantwoorden;
            int aantalAkkord;
            int aantalNietAkkord;
            int aantalGeskipt;
            if (this.SessieSpelMode == SessieSpelMode.Debat)
            {
                var stellingAntwoords = StellingAntwoorden.FindAll(s => s.Stelling.Equals(CurrentStelling));
                aantalantwoorden = stellingAntwoords.Count;
                aantalAkkord = stellingAntwoords.FindAll(a => a.isAkkoord == true).Capacity;
                aantalNietAkkord = stellingAntwoords.FindAll(a => a.isAkkoord == false).Capacity;
                aantalGeskipt = stellingAntwoords.FindAll(a => a.isBeantwoord == false).Capacity;
            }

            return "de gegevens van boven";
        }

        public void NotifyGebruikers()
        {
            foreach (var gebruiker in Gebruikers)
            {
                gebruiker.CurrentStelling = CurrentStelling;
            }
        }
        
    }
}