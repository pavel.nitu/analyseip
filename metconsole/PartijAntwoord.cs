﻿namespace metconsole
{
    public class PartijAntwoord
    {
        public string Argument { get; set; }
        public bool IsAkkoord { get; set; }
        public PartijStelling PartijStelling { get; set; }
        public Partij Partij { get; set; }
    }
}